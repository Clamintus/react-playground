import './App.css';
import LoginView from './components/macro/LoginView';
import login_view_settings from './testing/styleSettings.js';

function App() {

    const functions = {

        handle_submit: handleLogin,
        update_first: updateFirst,
        update_second: updateSecond
    }

    const feedback = {

        is_invalid_1: false,
        is_invalid_2: false,
        spinner: false,
        
        notification: {

            show: false,
            type: 3,
            message: "The message"
        }
    }

    return (

        <div className = "App">

            <LoginView functions = {functions} settings = {login_view_settings} feedback = {feedback}/>

        </div>
    );
}

function handleLogin(evt) {

    evt.preventDefault();
    console.log("handleLogin");
}

function updateFirst(evt) {

    evt.preventDefault();
    console.log("updateFirst");
}

function updateSecond(evt) {

    evt.preventDefault();
    console.log("updateSecond");
}

export default App;
