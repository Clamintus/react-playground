import React from "react";
import {BsKeyFill, BsPersonFill} from 'react-icons/bs';
import Notification from "../micro/Notification";
import "./login_view.css";

/*

    props:

    functions {

        handle_submit: fnc,
        update_first: fnc,
        update_second: fnc
    },

    settings {

        title: string,
        image_path: string,
        main_background: string,
        box_background: string,
        text_color: string,
        form_background: string,
        form_border_color: string,
        form_outline_color: string,
        icon_box_background: string,
        icon_color: string,
        button_background: string,
        button_border_color: string
    },

    feedback {

        is_invalid_1: boolean,
        is_invalid_2: boolean,
        spinner: boolean,
        
        notification {

            show: boolean
            type: int
            message: string
        }
    }

*/

function LoginView({functions, settings, feedback}) {

    return(

        <div className = "canvas f-col f-col-mid f-col-center"
            style = {{background: settings.main_background}}>

            <div className = "LoginView_main-box f-row">

                <div className = "LoginView_form-box f-row f-row-center flex-wrap"
                    style = {{backgroundColor: settings.box_background, alignContent: "space-between"}}>

                    <div className = "w-100">

                        <div className = "text-300 text-center"
                            style = {{color: settings.text_color}}>
                            
                            {settings.title}
                        </div>

                        <form onSubmit = {functions.handle_submit}>

                            <div className = "f-row f-row-right LoginView_form-wrapper">
                        
                                <div className = "f-col f-col-mid f-col-center box-border LoginView_icon-box"
                                    style = {{
                                        backgroundColor: settings.icon_box_background,
                                        borderColor: (feedback.is_invalid_1)? "#ff3535" : settings.form_border_color
                                    }}>  
                            
                                    <BsPersonFill size = {25} color = {settings.icon_color}/>
                        
                                </div>
                        
                                <div className = "w-100">

                                    <input className = "input text-100" type = "text" placeholder = "Username"
                                        onKeyUp = {functions.update_first}
                                        style = {{
                                            height: "45px", backgroundColor: settings.form_background,
                                            borderColor: (feedback.is_invalid_1)? "#ff3535" : settings.form_border_color,
                                            outlineColor: (feedback.is_invalid_1)? "#ff3535" : settings.form_outline_color,
                                            color: settings.text_color
                                        }}
                                    />
                        
                                </div>

                            </div>

                            <div className = "f-row f-row-right LoginView_form-wrapper">
                        
                                <div className = "f-col f-col-mid f-col-center box-border LoginView_icon-box"
                                    style = {{
                                        backgroundColor: settings.icon_box_background,
                                        borderColor: (feedback.is_invalid_2)? "#ff3535" : settings.form_border_color
                                    }}>  
                            
                                    <BsKeyFill size = {25} color = {settings.icon_color}/>
                        
                                </div>
                        
                                <div className = "w-100">

                                    <input className = "input text-100" type = "password" placeholder = "Password"
                                        onKeyUp = {functions.update_second}
                                        style = {{
                                            height: "45px", backgroundColor: settings.form_background,
                                            borderColor: (feedback.is_invalid_2)? "#ff3535" : settings.form_border_color,
                                            outlineColor: (feedback.is_invalid_2)? "#ff3535" : settings.form_outline_color,
                                            color: settings.text_color
                                        }}
                                    />
                        
                                </div>

                            </div>

                            <div className = "LoginView_form-wrapper">

                                <button className = "button text-150" type = "submit"
                                    style = {{
                                        height: "55px",
                                        background: settings.button_background,
                                        borderColor: settings.button_border_color,
                                        color: settings.text_color
                                    }}>

                                    Login

                                </button>

                            </div>

                        </form>

                        {
                            (feedback.spinner === true) &&
                            <div className = "LoginView_notification f-col f-col-center">

                                <span className = "loader"/>
                        
                            </div>
                        }

                        {
                            (feedback.notification.show === true) &&
                            <div className = "LoginView_notification">
                            
                                <Notification type = {feedback.notification.type} message = {feedback.notification.message}/>
                        
                            </div>
                        }

                    </div>

                    <a href = "/RegisterView">Forgot your password?</a>

                </div>

                <img className = "LoginView_image" src = {process.env.PUBLIC_URL + settings.image_path} alt = "login motif"/>

            </div>

        </div>
    );
}

export default LoginView;