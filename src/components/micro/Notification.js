import React from "react";

function Notification({type, message}) {

    switch(type) {

        case 0: return(<Success message = {message}/>);
        case 1: return(<Info message = {message}/>);
        case 2: return(<Warning message = {message}/>);
        default: return(<Danger message = {message}/>);
    }
}

function Success({message}) {

    return(

        <div className = "notification success">

            <b>Success!</b> {message}

        </div>
    );
}

function Info({message}) {

    return(

        <div className = "notification info">

            <b>Info!</b> {message}

        </div>
    );
}

function Warning({message}) {

    return(

        <div className = "notification warning">

            <b>Warning!</b> {message}

        </div>
    );
}

function Danger({message}) {

    return(

        <div className = "notification danger">

            <b>Error!</b> {message}

        </div>
    );
}

export default Notification;