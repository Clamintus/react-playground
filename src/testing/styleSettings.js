const login_view_settings = {

    title: "Login",
    image_path: "TestImage3.jpg",
    main_background: "linear-gradient(to bottom right, #005f7c, #008a78)",
    box_background: "var(--box_background_4)",
    text_color: "#ffffff",
    form_background: "#222222",
    form_border_color: "#005f7c",
    form_outline_color: "#008a78",
    icon_box_background: "#222222",
    icon_color: "#ffffff",
    button_background: "#222222",
    button_border_color: "#005f7c"
}

export default login_view_settings;